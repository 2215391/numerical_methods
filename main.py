from tkinter import messagebox

from mullers_method import *
from tkinter import *


def main():
    window = get_window()
    window.mainloop()


def get_window():
    window = Tk()
    window.config(background="#8683ce")
    window.geometry("600x600")
    window.title("Muller's Method")
    window.resizable(False, False)

    window.grid_rowconfigure(2, weight=1)
    window.grid_columnconfigure(1, weight=1)

    function_label = Label(text="Function", font="Helvetica, 14")
    starting_values_label = Label(text="Starting Values", font="Helvetica 14")

    x0_label = Label(window, text="x0", font="Helvetica 12")
    x1_label = Label(window, text="x1", font="Helvetica 12")
    x2_label = Label(window, text="x2", font="Helvetica 12")
    iterations_label = Label(window, text="Number of Iterations", font="Helvetica 14")
    approximate_error_label = Label(window, text="Approximate Error", font="Helvetica 14")

    function_entry = Entry(window, width=30, font="Helvetica 11")
    function_entry.insert(0, "x^3-x-1")
    iterations_entry = Entry(window, width=30, font="Helvetica 11")
    approximate_error_entry = Entry(window, width=30, font="Helvetica 11")
    x0_entry = Entry(window, font="Helvetica 11")
    x1_entry = Entry(window, font="Helvetica 11")
    x2_entry = Entry(window, font="Helvetica 11")

    calculate_button = Button(text="Calculate", command=lambda: show_results(
        solve(function_entry.get(), float(x0_entry.get()), float(x1_entry.get()),
              float(x2_entry.get()), float(approximate_error_entry.get()),
              int(iterations_entry.get()))))

    function_label.grid(row=0, column=0, padx=20, pady=20)
    iterations_label.grid(row=1, column=0, padx=20, pady=20)
    approximate_error_label.grid(row=2, column=0, pady=20, padx=20)
    starting_values_label.grid(row=3, column=0, padx=20, pady=20)

    x0_label.grid(row=4, column=0, padx=20, pady=20)
    x1_label.grid(row=5, column=0, padx=20, pady=20)
    x2_label.grid(row=6, column=0, padx=20, pady=20)

    function_entry.grid(row=0, column=1, pady=20, padx=20)
    iterations_entry.grid(row=1, column=1, pady=20, padx=20)
    approximate_error_entry.grid(row=2, column=1, padx=20, pady=20)
    x0_entry.grid(row=4, column=1, padx=20, pady=20)
    x1_entry.grid(row=5, column=1, padx=20, pady=20)
    x2_entry.grid(row=6, column=1, padx=20, pady=20)

    calculate_button.grid(row=7, column=0, columnspan=2, padx=20, pady=20)
    calculate_button.config(width=10, height=2, font="Helvetica 12")

    return window


def show_results(results):
    results_window = Tk()
    results_window.title("Results")
    results_window.config(background="#8683ce")

    results_window.resizable(False, False)
    results_window.geometry("600x600")
    scroll_bar = Scrollbar(results_window, orient="horizontal")
    results_text = Text(results_window, font="Helvetica, 10",
                        xscrollcommand=scroll_bar.set, width=600, height=600)
    results_text.insert(END, results)
    results_text.config(background="#86e3ce")
    results_text.config(font="Helvetica 15")
    results_text.pack(side=TOP, fill=X, padx=10)
    scroll_bar.config(command=results_text.xview)
    scroll_bar.pack(side=BOTTOM, fill=X)
    results_window.mainloop()


if __name__ == '__main__':
    main()
