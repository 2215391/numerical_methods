import math


def solve(function, x0, x1, x2, stopping_criteria, max_iterations):
    result = ""
    current_iteration = 1
    while current_iteration <= max_iterations:
        result += ("______________________Iteration " + str(current_iteration) +
                   "______________________") + "\n"

        h0 = round(get_h0(x1, x0), 6)
        result += ("h0 = " + str(h0)) + "\n"
        h1 = round(get_h1(x2, x1), 6)
        result += ("h1 = " + str(h1)) + "\n"
        delta0 = round(get_delta0(function, h0, x0, x1), 6)
        result += ("delta0 = " + str(delta0)) + "\n"
        delta1 = round(get_delta1(function, h1, x1, x2), 6)
        result += ("delta1 = " + str(delta1)) + "\n"
        a = round(get_a(delta0, delta1, h0, h1), 6)
        result += ("a = " + str(a)) + "\n"
        b = round(get_b(a, h1, delta1), 6)
        result += ("b = " + str(b)) + "\n"
        c = round(get_c(function, x2), 6)
        result += ("c = " + str(c)) + '\n'
        x3 = round(get_x3(x2, a, b, c), 6)
        result += ("x3 = " + str(x3)) + '\n'
        current_approximate_error = get_approximate_error(x3, x2)
        result += ("approximate error: " + str(current_approximate_error)) + "\n"
        if current_approximate_error < stopping_criteria:
            break
        x0 = x1
        x1 = x2
        x2 = x3
        current_iteration += 1
    print(result)
    return result


def get_h0(x1, x0):
    return x1 - x0


def get_h1(x2, x1):
    return x2 - x1


def get_delta0(function, h0, x0, x1):
    return (get_function_value(function, x1) - get_function_value(function, x0)) / h0


def get_delta1(function, h1, x1, x2):
    return (get_function_value(function, x2) - get_function_value(function, x1)) / h1


def get_a(delta0, delta1, h0, h1):
    return (delta1 - delta0) / (h0 + h1)


def get_b(a, h1, delta1):
    return a * h1 + delta1


def get_c(function, x2):
    return get_function_value(function, x2)


def get_function_value(function, value):
    fun = function.replace("-x", "-1x")
    fun = fun.replace("+x", "+1x")
    fun = fun.replace("x", "*x")
    if fun[0] == "*":
        fun = fun[1:]
    fun = fun.replace("x", str(value))
    fun = fun.replace("^", "**")
    return eval(fun)


def get_x3(x2, a, b, c):
    positive = x2 + (-2 * c / (b + math.sqrt(b ** 2 - 4 * a * c)))
    negative = x2 + (-2 * c / (b - math.sqrt(b ** 2 - 4 * a * c)))
    return max(positive, negative)


def get_approximate_error(x3, x2):
    return round(abs((x3 - x2) / x3) * 100, 2)
